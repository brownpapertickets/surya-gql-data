import { QueryParser } from "../src/queryParser"

let queryParser
beforeAll(() => {
  queryParser = new QueryParser()
})

function checkPredicate(node, type, field, value, fieldParams = null) {
  // predicate type
  expect(node.type).toEqual(type)

  // field
  const fieldNode = node.children[0]
  expect(fieldNode.type).toEqual("field")
  const fieldIdentifier = fieldNode.children[0]
  expect(fieldIdentifier.type).toEqual("identifier")
  expect(fieldIdentifier.value).toEqual(field)

  // value
  expect(node.children[1].value).toEqual(value)

  // opt test for field params
  if (fieldParams && Array.isArray(fieldParams)) {
    const fieldParamsNode = fieldNode.children[1]
    expect((fieldParamsNode.type = "paramList"))
    const paramNodes = fieldParamsNode.children
    for (let i = 0; i < fieldParams.length; i++) {
      const paramVal = paramNodes[i].value
      // console.log(`Param: ${paramVal}`)
      expect(fieldParams[i]).toEqual(paramVal)
    }
  }
}

function getFieldIdentifier(node) {
  expect(node.type).toEqual("field")
  const identifierNode = node.children[0]
  expect(identifierNode.type).toEqual("identifier")
  return identifierNode.value
}

function checkSort(node, asserts) {
  expect(node.type).toEqual("sortSpecList")

  for (let n = 0; n < asserts.length; n++) {
    let assert = asserts[n]
    let sortNode = node.children[n]

    expect(sortNode.type).toEqual("sortSpec")

    const sortFieldNode = sortNode.children[0]
    expect(sortFieldNode.type).toEqual("field")
    const fieldIdentifierNode = sortFieldNode.children[0]
    expect(fieldIdentifierNode.type).toEqual("identifier")
    expect(fieldIdentifierNode.value).toEqual(assert.field)

    if (assert.params) {
      const paramListNode = sortFieldNode.children[1]
      expect(paramListNode.type).toEqual("paramList")
      const paramVals = paramListNode.children.map((item) => item.value)
      console.log(
        `checkSort params found: ${JSON.stringify(
          paramVals
        )} expected: ${JSON.stringify(assert.params)}`
      )
      expect(assert.params).toEqual(paramVals)
    }

    if (assert.dir) {
      expect(sortNode.children[1].type).toEqual(assert.dir)
    }
  }
}

describe("queryParser filter tests", () => {
  test("equals String Predicate", () => {
    const root = queryParser.parseFilter("name = 'breanna'")
    checkPredicate(root, "equalPredicate", "name", "breanna")
  })

  test("equals Number Predicate", () => {
    const root = queryParser.parseFilter("height = 6.5 ")
    checkPredicate(root, "equalPredicate", "height", 6.5)
  })

  test("Not Equals String Predicate", () => {
    const root = queryParser.parseFilter("name != 'nobody'")
    checkPredicate(root, "notEqualPredicate", "name", "nobody")
  })

  test("Not Equals Float Number Predicate", () => {
    const root = queryParser.parseFilter("pi != 3.14159")
    checkPredicate(root, "notEqualPredicate", "pi", 3.14159)
  })

  test("Not Equals Integer Number Predicate", () => {
    const root = queryParser.parseFilter("three != 3")
    checkPredicate(root, "notEqualPredicate", "three", 3)
  })

  // greaterEqualPredicate |
  test("Greater Equals Number Predicate", () => {
    const root = queryParser.parseFilter("age >= 21")
    checkPredicate(root, "greaterEqualPredicate", "age", 21)
  })

  // greaterPredicate |
  test("Greater Number Predicate", () => {
    const root = queryParser.parseFilter("quantity > 100")
    checkPredicate(root, "greaterPredicate", "quantity", 100)
  })
  // lessEqualPredicate |
  test("Less Equal Number Predicate", () => {
    const root = queryParser.parseFilter("quantity <= 0")
    checkPredicate(root, "lessEqualPredicate", "quantity", 0)
  })

  //lessPredicate |
  test("Less Than Number Predicate, negative integer", () => {
    const root = queryParser.parseFilter("diff < -1")
    checkPredicate(root, "lessPredicate", "diff", -1)
  })

  //lessPredicate |
  test("Less Than string", () => {
    const root = queryParser.parseFilter("lastName < 'AAA' ")
    checkPredicate(root, "lessPredicate", "lastName", "AAA")
  })

  //matchesPredicate |
  test("matches Predicate lowercase", () => {
    const root = queryParser.parseFilter("email matches '@google.com$' ")
    checkPredicate(root, "matchesPredicate", "email", "@google.com$")
  })

  test("matches Predicate uppercase", () => {
    const root = queryParser.parseFilter("firstName MATCHES '^[ 0-9_]' ")
    checkPredicate(root, "matchesPredicate", "firstName", "^[ 0-9_]")
  })

  //existsPredicate |
  test("exists Predicate lowercase", () => {
    const root = queryParser.parseFilter("address exists ")
    expect(root.type).toEqual("existsPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("address")
    expect(root.children[1]).toBeUndefined()
  })

  test("exists Predicate uppercase", () => {
    const root = queryParser.parseFilter("emailToken EXISTS ")
    expect(root.type).toEqual("existsPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("emailToken")
    expect(root.children[1]).toBeUndefined()
  })

  //notExistsPredicate |
  test("not exists Predicate lowercase", () => {
    const root = queryParser.parseFilter("address !exists ")
    expect(root.type).toEqual("notExistsPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("address")
    expect(root.children[1]).toBeUndefined()
  })

  test("not exists Predicate uppercase", () => {
    const root = queryParser.parseFilter("emailToken !EXISTS ")
    expect(root.type).toEqual("notExistsPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("emailToken")
    expect(root.children[1]).toBeUndefined()
  })

  //inPredicate |
  test("in Predicate lowercase", () => {
    const root = queryParser.parseFilter("state in ( 'wa', 'or', 'ca', 'mt' ) ")
    expect(root.type).toEqual("inPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("state")
    expect(root.children.length).toEqual(5)
    expect(root.children[1].type).toEqual("stringLiteral")
    expect(root.children[1].value).toEqual("wa")
  })

  test("in Predicate uppercase, numeric", () => {
    const root = queryParser.parseFilter("code IN( 1, 3, 12, 1024)  ")
    expect(root.type).toEqual("inPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("code")
    expect(root.children.length).toEqual(5)
    expect(root.children[4].type).toEqual("number")
    expect(root.children[4].value).toEqual(1024)
  })

  //notInPredicate ;
  test("!in Predicate lowercase", () => {
    const root = queryParser.parseFilter("state !in ( 'nd', 'sd' ) ")
    expect(root.type).toEqual("notInPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("state")
    expect(root.children.length).toEqual(3)
    expect(root.children[1].type).toEqual("stringLiteral")
    expect(root.children[1].value).toEqual("nd")
  })

  test("in Predicate uppercase, numeric, mixed", () => {
    const root = queryParser.parseFilter("code !IN( -3,12, true)  ")
    expect(root.type).toEqual("notInPredicate")
    const fieldName = getFieldIdentifier(root.children[0])
    expect(fieldName).toEqual("code")
    expect(root.children.length).toEqual(4)
    expect(root.children[1].type).toEqual("number")
    expect(root.children[1].value).toEqual(-3)
    expect(root.children[3].type).toEqual("trueLiteral")
    expect(root.children[3].value).toEqual(true)
  })

  test("trimQuotes with no quotes", () => {
    expect(queryParser.trimQuotes("blah")).toBe("blah")
  })

  // Fail scnearios -----------------------------------------
  test("invalid predicate '&&' ", () => {
    try {
      const root = queryParser.parseFilter("name && 'nobody'")
      expect(true).toBe(false)
    } catch (ex) {
      console.log(`Expect to fail: ${ex.message}`)
      // take this branch!
      expect(true).toBe(true)
    }
  })

  test("missing root node", () => {
    expect(() => queryParser.normalizeAST({ name: "", tokens: [] })).toThrow(
      Error
    )
  })

  // Moot parens queryExpression one predicate

  test("Moot parens around simple pred", () => {
    const root = queryParser.parseFilter("name = 'breanna'")
    checkPredicate(root, "equalPredicate", "name", "breanna")
    const root2 = queryParser.parseFilter("( name = 'breanna' )")
    expect(root).toEqual(root2)
  })

  // BOOL combiners -----------------------------------------

  // AND -------------
  test("pred AND pred test ", () => {
    const root = queryParser.parseFilter(
      "firstName = 'breanna' AND lastName != 'smith' "
    )
    // console.log(JSON.stringify(root, null, 2))
    const op1 = root.children[0]
    const op2 = root.children[1]

    expect(root.type).toEqual("andClause")
    checkPredicate(op1, "equalPredicate", "firstName", "breanna")
    checkPredicate(op2, "notEqualPredicate", "lastName", "smith")
  })

  test("pred AND pred test lower case ", () => {
    const root = queryParser.parseFilter(
      "age > 21 and refreshments = 'alcohol' "
    )

    expect(root.type).toEqual("andClause")
    checkPredicate(root.children[0], "greaterPredicate", "age", 21)
    checkPredicate(
      root.children[1],
      "equalPredicate",
      "refreshments",
      "alcohol"
    )
  })

  test("pred multiple AND ", () => {
    const root = queryParser.parseFilter(
      "firstName = 'John' AND lastName matches 'lyon-' AND age < 30  "
    )

    expect(root.type).toEqual("andClause")
    checkPredicate(root.children[0], "equalPredicate", "firstName", "John")
    const and2 = root.children[1]
    expect(and2.type).toEqual("andClause")
    checkPredicate(and2.children[0], "matchesPredicate", "lastName", "lyon-")
    checkPredicate(and2.children[1], "lessPredicate", "age", 30)
  })

  test("Moot parens around ( pred AND pred ) test ", () => {
    const root = queryParser.parseFilter(
      "firstName = 'breanna' AND lastName != 'smith' "
    )
    // console.log(JSON.stringify(root, null, 2))
    const op1 = root.children[0]
    const op2 = root.children[1]

    expect(root.type).toEqual("andClause")
    checkPredicate(op1, "equalPredicate", "firstName", "breanna")
    checkPredicate(op2, "notEqualPredicate", "lastName", "smith")
    // same but with parens
    const root2 = queryParser.parseFilter(
      "( firstName = 'breanna' AND lastName != 'smith' ) "
    )
    expect(root).toEqual(root2)
  })

  // OR ---------------
  test("eqpred OR eqpred test ", () => {
    const root = queryParser.parseFilter("state = 'WA' OR state = 'OR'")
    expect(root.type).toEqual("orClause")
    checkPredicate(root.children[0], "equalPredicate", "state", "WA")
    checkPredicate(root.children[1], "equalPredicate", "state", "OR")
  })

  test("eqpred OR eqpred true, false, test", () => {
    const root = queryParser.parseFilter("approved = true OR active = false ")
    expect(root.type).toEqual("orClause")
    checkPredicate(root.children[0], "equalPredicate", "approved", true)
    checkPredicate(root.children[1], "equalPredicate", "active", false)
  })

  test("pred multiple OR ", () => {
    const root = queryParser.parseFilter(
      "country = 'US' OR override != true OR currency = 'Euros'  "
    )

    //console.log(JSON.stringify(root, null, 2))
    expect(root.type).toEqual("orClause")
    checkPredicate(root.children[0], "equalPredicate", "country", "US")
    const and2 = root.children[1]
    expect(and2.type).toEqual("orClause")
    checkPredicate(and2.children[0], "notEqualPredicate", "override", true)
    checkPredicate(and2.children[1], "equalPredicate", "currency", "Euros")
  })

  // AND/OR precidence
  test(" a AND b OR c AND d ", () => {
    const root = queryParser.parseFilter(
      "firstName = 'breanna' AND lastName = 'anderson' OR firstName = 'john' AND lastName = 'lyon-smith' "
    )

    //console.log(JSON.stringify(root, null, 2))
    expect(root.type).toEqual("orClause")
    const and1 = root.children[0]
    const and2 = root.children[1]

    expect(and1.type).toEqual("andClause")
    expect(and1.type).toEqual("andClause")

    checkPredicate(and1.children[0], "equalPredicate", "firstName", "breanna")
    checkPredicate(and1.children[1], "equalPredicate", "lastName", "anderson")
    checkPredicate(and2.children[0], "equalPredicate", "firstName", "john")
    checkPredicate(and2.children[1], "equalPredicate", "lastName", "lyon-smith")
  })

  test(" a OR  b AND c OR d ", () => {
    const root = queryParser.parseFilter(
      "firstName = 'johny' OR firstName = 'jimmy' AND lastName = 'smith' OR firstName = 'jane' "
    )

    expect(root.type).toEqual("orClause")
    const or1left = root.children[0]
    const or1right = root.children[1]

    checkPredicate(or1left, "equalPredicate", "firstName", "johny")
    expect(or1right.type).toEqual("orClause")

    const or2left = or1right.children[0]
    const or2right = or1right.children[1]

    expect(or2left.type).toEqual("andClause")

    checkPredicate(or2left.children[0], "equalPredicate", "firstName", "jimmy")
    checkPredicate(or2left.children[1], "equalPredicate", "lastName", "smith")

    checkPredicate(or2right, "equalPredicate", "firstName", "jane")
  })

  test(" ( a OR  b ) AND ( c OR d ) ", () => {
    const root = queryParser.parseFilter(
      "( firstName = 'johny' OR firstName = 'jimmy' ) AND ( lastName = 'smith' OR lastName matches 'jane' ) "
    )

    // console.log(JSON.stringify(root, null, 2))
    expect(root.type).toEqual("andClause")
    const andLeft = root.children[0]
    const andRight = root.children[1]

    expect(andLeft.type).toEqual("orClause")
    expect(andRight.type).toEqual("orClause")

    checkPredicate(andLeft.children[0], "equalPredicate", "firstName", "johny")
    checkPredicate(andLeft.children[1], "equalPredicate", "firstName", "jimmy")

    checkPredicate(andRight.children[0], "equalPredicate", "lastName", "smith")
    checkPredicate(andRight.children[1], "matchesPredicate", "lastName", "jane")
  })
})

describe("queryParser sort tests", () => {
  test("one default", () => {
    const root = queryParser.parseSort("firstName")

    checkSort(root, [{ field: "firstName", dir: null }])
  })

  test("asc", () => {
    const root = queryParser.parseSort("date asc")

    checkSort(root, [{ field: "date", dir: "asc" }])
  })

  test("ASC", () => {
    const root = queryParser.parseSort("value ASC")

    checkSort(root, [{ field: "value", dir: "asc" }])
  })

  test("desc", () => {
    const root = queryParser.parseSort("value desc")

    checkSort(root, [{ field: "value", dir: "desc" }])
  })

  test("DESC", () => {
    const root = queryParser.parseSort("address.state DESC")

    checkSort(root, [{ field: "address.state", dir: "desc" }])
  })

  test("list default", () => {
    const root = queryParser.parseSort("type, lastName")

    checkSort(root, [
      { field: "type", dir: null },
      { field: "lastName", dir: null },
    ])
  })

  test("list default 3", () => {
    const root = queryParser.parseSort("type, created, city")

    checkSort(root, [
      { field: "type", dir: null },
      { field: "created", dir: null },
      { field: "city", dir: null },
    ])
  })

  test("list ASC 3", () => {
    const root = queryParser.parseSort("type asc, created ASC, city")

    checkSort(root, [
      { field: "type", dir: "asc" },
      { field: "created", dir: "asc" },
      { field: "city", dir: null },
    ])
  })

  test("list ASC 3", () => {
    const root = queryParser.parseSort(
      "count DESC, created DESC, agent.owner desc"
    )

    checkSort(root, [
      { field: "count", dir: "desc" },
      { field: "created", dir: "desc" },
      { field: "agent.owner", dir: "desc" },
    ])
  })

  test("parameterized sort", () => {
    const root = queryParser.parseSort("dateActive('month') ASC")
  })

  test("parameterized sort 2", () => {
    const root = queryParser.parseSort("zipcode.distanceMiles(-102.2, 48.7)")

    checkSort(root, [
      { field: "zipcode.distanceMiles", dir: null, params: [-102.2, 48.7] },
    ])
  })

  test("parameterized sort 3", () => {
    const root = queryParser.parseSort(
      "startDate DESC, zipcode.distance(-102.2, 48.7, 'km', true )"
    )

    checkSort(root, [
      { field: "startDate", dir: "desc" },
      {
        field: "zipcode.distance",
        dir: null,
        params: [-102.2, 48.7, "km", true],
      },
    ])
  })
})

describe("queryParser param fields", () => {
  test("test id with args", () => {
    const root = queryParser.parseFilter("distanceMiles(-102.5, 80.2) < 50 ")
    // console.log(JSON.stringify(root))
    checkPredicate(root, "lessPredicate", "distanceMiles", 50, [-102.5, 80.2])
  })

  test("test joined field with args", () => {
    const root = queryParser.parseFilter(
      "zipcode.distanceMiles(-102.5, 80.2) >= 50 "
    )

    checkPredicate(root, "greaterEqualPredicate", "zipcode.distanceMiles", 50, [
      -102.5,
      80.2,
    ])
  })
})
