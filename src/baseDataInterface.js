export class BaseDataInterface {
  constructor(container) {
    this.log = container.log
    this.typeSpecsMap = {}
    this.apis = {}
  }

  async loadTypes(typelist) {
    this.typeSpecsMap = {}
    typelist.forEach(t => {
      const type = new t()
      const typeSpec = type.typeSpec()
      this.typeSpecsMap[typeSpec.name] = typeSpec
    })
  }

  async loadAPI(typeName, apiClass, mainTypeClass, config) {
    const apiContainer = { db: this, log: this.log }
    const api = new apiClass(
      apiContainer,
      new mainTypeClass(),
      this.typeSpecsMap,
      config
    )
    this.apis[typeName] = api
    return api
  }

  getAPI(typeName) {
    return this.apis[typeName]
  }

  //>>> Primarily for testing purposes
  getAllAPIs() {
    return this.apis
  }

  getTypeSpecs() {
    return this.typeSpecsMap
  }
  // <<<

  /**
   * Override this function to connect to data store
   * @param {*} connectionString
   * @param {*} isProduction
   */
  async connect(connectionString, isProduction) {
    throw new Error(`Not Implemented`)
  }

  /**
   * Override this function to disconnect from data store
   */
  async disconnect() {
    throw new Error(`Not Implemented`)
  }
}
