import rdparser from "recursive-descent"
import filterGrammar from "./filterGrammar"
import sortGrammar from "./sortGrammar"

const EOF = "#eof"
const STRING_LITERAL = "stringLiteral"
const NUMBER_LITERAL = "number"
const TRUE_LITERAL = "trueLiteral"
const FALSE_LITERAL = "falseLiteral"

export class QueryParser {
  constructor() {
    this.filterRules = rdparser.bnfParse(filterGrammar)
    this.sortRules = rdparser.bnfParse(sortGrammar)
  }

  parseFilter(query) {
    const ast = this.parseFilterTokens(query)
    return this.normalizeAST(ast)
  }

  parseFilterTokens(query) {
    const ast = rdparser.parse(this.filterRules, query)
    // console.log(JSON.stringify(ast, null, 2))
    // rdparser.print(ast)
    return ast
  }

  parseSort(sortSpec) {
    const ast = this.parseSortTokens(sortSpec)
    const root = this.normalizeAST(ast)
    return root
  }

  parseSortTokens(sortSpec) {
    return rdparser.parse(this.sortRules, sortSpec)
  }

  /**
   * reduce ast format to compast object property format.
   * @param {*} ast
   */
  normalizeAST(ast) {
    if (ast.name != "program" || ast.tokens.length == 0) {
      throw Error("invalid AST: program root node missing")
    }

    const normalAst = this.parseToken(ast.tokens[0])
    // console.log(JSON.stringify(normalAst, null, 2))

    return normalAst
  }

  parseToken(token) {
    const nodeType = token.name
    let node = {}
    if (token.tokens.length > 0) {
      let children = []
      for (let child of token.tokens) {
        children.push(this.parseToken(child))
      }
      node = { type: nodeType, children }
    } else {
      let value = token.value || null
      switch (nodeType) {
        case STRING_LITERAL:
          value = this.trimQuotes(value)
          break
        case NUMBER_LITERAL:
          value = parseFloat(value)
          break
        case TRUE_LITERAL:
          value = true
          break

        case FALSE_LITERAL:
          value = false
          break
      }
      node = { type: nodeType, value }
    }
    return node
  }

  trimQuotes(value) {
    if (value.charAt(0) == "'" && value.charAt(value.length - 1) == "'") {
      return value.substring(1, value.length - 1)
    }
    return value
  }
}
