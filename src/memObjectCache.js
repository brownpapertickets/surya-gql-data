/**
 * In-memory cache for GraphQL Objects
 */

// const NodeCache = require("node-cache")
import NodeCache from "node-cache"
import { QueryParser } from "./queryParser"
import md5 from "md5"

export class MemObjectCache {
  constructor(container, type, ttl, checkPeriod) {
    this.log = container.log
    this.type = type
    this.queryParser = new QueryParser()
    this.cache = new NodeCache({ stdTTL: ttl, checkperiod: checkPeriod })
    this.cache.on("expired", (key, value) => {
      // console.log(`Client cache expired: ${key}`)
    })
  }

  getObjKey(id) {
    return `:O:${id}`
  }

  async setObject(id, value) {
    const objKey = this.getObjKey(id)
    const sval = JSON.stringify(value) // cache can't store complex object
    return new Promise((resolve, reject) => {
      this.cache.set(objKey, sval, (err, success) => {
        if (err) {
          reject(err)
        }
        resolve(success)
      })
    })
  }

  async getObject(id) {
    const objKey = this.getObjKey(id)
    // this.log.info(`getObject key:${objKey} id: ${id}`)
    return new Promise((resolve, reject) => {
      this.cache.get(objKey, (err, value) => {
        if (err) {
          this.log.error("get object cache error")
          reject(err)
        }
        if (value) {
          // this.log.info("get object cache hit")
          let obj = JSON.parse(value)
          obj = Object.create(this.type).loadData(obj) // turn into instance of object type with all methods and goodies
          resolve(obj)
        } else {
          // empty result, cache miss
          // this.log.info("get object cache miss")
          resolve(value)
        }
      })
    })
  }

  /**
   * Remove object from cache due to modification or deletion
   * TODO: Keep inversion lists to purge query results that include this object
   * @param {} id
   */
  async purgeObject(id) {
    const objKey = this.getObjKey(id)
    return new Promise((resolve, reject) => {
      this.cache.del(objKey, (err, count) => {
        if (err) {
          this.log.error("delete object cache error")
          reject(err)
        }
        resolve(count)
      })
    })
  }

  getQueryKey(offset, limit, filter, sort) {
    let pfilter =
      filter.trim().length > 0 ? this.queryParser.parseFilter(filter) : ""
    if (pfilter) {
      pfilter = JSON.stringify(pfilter)
      //this.log.info(pfilter)
    }

    let psort = sort.trim().length > 0 ? this.queryParser.parseSort(sort) : ""
    if (psort) {
      psort = JSON.stringify(psort)
      //this.log.info(psort)
    }
    const concat = `Off:${offset}-Lim:${limit}-fil:${pfilter}-sort:${psort}`
    const hash = md5(concat)
    //this.log.info(`query hash: ${hash}`)
    return `Q:${hash}`
  }
  async setQueryResult(offset, limit, filter, sort, result) {
    const queryKey = this.getQueryKey(offset, limit, filter, sort)
    // this.log.info(`>> SetQueryResult queryKey: ${queryKey} `)
    const sresult = JSON.stringify(result) // cache can't store complex object
    return new Promise((resolve, reject) => {
      this.cache.set(queryKey, sresult, (err, success) => {
        if (err) {
          reject(err)
        }
        resolve(success)
      })
    })
  }

  async getQueryResult(offset, limit, filter, sort) {
    const queryKey = this.getQueryKey(offset, limit, filter, sort)

    return new Promise((resolve, reject) => {
      this.cache.get(queryKey, (err, value) => {
        if (err) {
          this.log.error("get query cache error")
          reject(err)
        }
        if (value) {
          // this.log.info("get query cache hit")
          let results = JSON.parse(value)
          let items = results.items.map(item => {
            return Object.create(this.type).loadData(item)
          })
          results.items = items
          resolve(results)
        } else {
          // empty result, cache miss
          // this.log.info("get query cache miss")
          resolve(value)
        }
      })
    })
  }
}
