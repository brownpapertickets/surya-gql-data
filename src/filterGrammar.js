const filterGrammar = String.raw`

    //special ignore token to allow and ignore whitespace
    ignore = ~[\s]+~ ;

    number = ~(\-)?[0-9]+(\.[0-9]+)?~ ;

    // Language Tokens
    // Bool Operators
    andBool = ~(AND|and)~ ;
    orBool = ~(OR|or)~ ;
    notOp = ~(NOT|not)~ ;

    lParen = ~\(~ ;
    rParen = ~\)~ ;

    // comparison operators
    eqOp = ~=~ ;
    neOp = ~!=~ ;
    gtOp = ~>~ ;
    ltOp = ~<~ ;
    gteOp = ~>=~ ;
    lteOp = ~<=~ ;
    matchesOp = ~(MATCHES|matches)~ ;
    existsOp = ~(EXISTS|exists)~ ;
    notExistsOp = ~!(EXISTS|exists)~ ;
    inOp = ~(IN|in)~ ;
    notInOp = ~(!IN|!in|NIN|nin)~ ;
    trueLiteral = ~true~ ;
    falseLiteral = ~false~ ;
    comma = ~,~ ;

    identifier = ~[a-zA-Z_][0-9a-zA-Z_\-\.]{0,100}~ ;
    stringLiteral = ~'([^'\\]*(?:\\.[^'\\]*)*)'~ ;


    program : < filterExpression ;

    booleanLiteral: trueLiteral | falseLiteral ;

    filterExpression : @ orClause | < filterTerm ;

    orClause: @ < filterTerm < orBool < orRight ;

    orRight: @ orClause | < filterTerm ;

    filterTerm : @ andClause | < filterAtom ;

    andClause: @ < filterAtom < andBool < andRight ;

    andRight: @andClause | < filterAtom ;

    filterAtom: @ notExpression |  < lParen < filterExpression < rParen | < predicate ;

    notExpression :  < notOp < lParen < filterExpression < rParen ;

    predicate :
      equalPredicate |
      notEqualPredicate |
      greaterEqualPredicate |
      greaterPredicate |
      lessEqualPredicate |
      lessPredicate |
      matchesPredicate |
      existsPredicate |
      notExistsPredicate |
      inPredicate |
      notInPredicate ;

    equalPredicate : @ field < eqOp < literal ;
    notEqualPredicate : @ field  < neOp < literal ;
    greaterEqualPredicate : @ field  < gteOp < literal ;
    greaterPredicate : @ field  < gtOp < literal ;
    lessEqualPredicate : @ field  < lteOp < literal ;
    lessPredicate : @ field  < ltOp < literal ;
    matchesPredicate : @ field < matchesOp stringLiteral ;
    existsPredicate : @ field < existsOp ;
    notExistsPredicate : @ field < notExistsOp ;
    inPredicate : @ field < inOp < lParen < literalList < rParen ;
    notInPredicate : @ field < notInOp <  lParen < literalList < rParen ;

    literalList : < literal < ( < comma < literal ) = moreliterals *  ;

    literal : stringLiteral | number | < booleanLiteral ;

    field: < paramField |
           < simpleField  ;

    paramField: @ identifier paramList ;
    simpleField: @ identifier ;

    paramList: <lParen < literalList < rParen ;

`
module.exports = filterGrammar
